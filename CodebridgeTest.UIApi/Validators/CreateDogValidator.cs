﻿using CodebridgeTest.Core.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodebridgeTest.UIApi.Validators
{
    public class CreateDogValidator: AbstractValidator<CreateDog>
    {
        public CreateDogValidator()
        {
            RuleFor(x => x).NotEmpty();

            When(x => x != null, () => {
                RuleFor(x => x.Color).NotEmpty().MaximumLength(100).WithName(nameof(CreateDog.Color).ToLower());
            });

            When(x => x != null, () => {
                RuleFor(x => x.Name).NotEmpty().MaximumLength(100).WithName(nameof(CreateDog.Name).ToLower());
            });

            When(x => x != null, () => {
                RuleFor(x => x.TailLength).GreaterThan(0).WithName("tail_length");
            });

            When(x => x != null, () => {
                RuleFor(x => x.Weight).GreaterThan(0).WithName(nameof(CreateDog.Weight).ToLower());
            });
        }
    }
}
