﻿using CodebridgeTest.Core.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodebridgeTest.UIApi.Validators
{
    public class DogSearchQueryValidator: AbstractValidator<DogSearchQuery>
    {
        public DogSearchQueryValidator()
        {
            When(x => x != null && !string.IsNullOrWhiteSpace(x.Attribute), () =>
            {
                RuleFor(x => x.Attribute).Custom((attribute, context) => {
                    var allowedAttribute = new string[] { "name", "color", "tail_length", "weight" };

                    if (!allowedAttribute.Any(d => d == attribute.ToLower()))
                    {
                        context.AddFailure("Please enter a valid attribute. e.g name, color, tail_length, weight.");
                    }
                });
            });

            When(x => x != null && !string.IsNullOrWhiteSpace(x.Order), () =>
            {
                RuleFor(x => x.Order).Custom((order, context) => {
                    var allowedOrder = new string[] { "asc", "desc" };

                    if (!allowedOrder.Any(d => d == order.ToLower()))
                    {
                        context.AddFailure("Please enter a valid order. e.g asc, desc.");
                    }
                });
            });

            When(x => x != null, () =>
            {
                RuleFor(x => x.PageNumber).GreaterThan(0);
                RuleFor(x => x.PageSize).GreaterThan(0);
            });
        }
    }
}
