﻿using CodebridgeTest.Core.Exceptions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace CodebridgeTest.UIApi.Middlewares
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IWebHostEnvironment _env;
        private readonly ILogger _logger;

        public ErrorHandlingMiddleware(RequestDelegate next, IWebHostEnvironment env, ILogger logger)
        {
            _next = next;
            _env = env;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleApplicationExceptionErrors(context, ex);
            }
        }

        private async Task HandleApplicationExceptionErrors(HttpContext context, Exception exception)
        {
            object error = null;

            switch (exception)
            {
                case AppException ex:
                    context.Response.StatusCode = ex.StatusCode;

                    if (_env.IsDevelopment())
                    {
                        error = new { ex.StatusCode, Errors = new string[] { ex.FriendlyMessage } };
                    }
                    else
                    {
                        error = new { ex.StatusCode, Errors = new string[] { ex.FriendlyMessage }, ex.StackTrace };
                    }

                    _logger.Error("Application Exception. Ex: {Exception}, Err");
                    break;
                case Exception ex:
                    context.Response.StatusCode = 500;

                    if (_env.IsDevelopment())
                    {
                        error = new { StatusCode = StatusCodes.Status500InternalServerError, Errors = new string[] { ex.Message } };
                    }
                    else
                    {
                        error = new { StatusCode = StatusCodes.Status500InternalServerError, Errors = new string[] { ex.Message }, ex.StackTrace };
                    }

                    break;
            }

            context.Response.ContentType = "application/json";

            if (error != null)
            {
                var namingPolicy = new JsonSerializerOptions
                {
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                    DictionaryKeyPolicy = JsonNamingPolicy.CamelCase,
                };

                var result = System.Text.Json.JsonSerializer.Serialize(error, namingPolicy);
                await context.Response.WriteAsync(result);
            }
        }
    }
}
