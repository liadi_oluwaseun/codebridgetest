﻿using CodebridgeTest.Core.Interfaces.Service;
using CodebridgeTest.Core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodebridgeTest.UIApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DogController : ControllerBase
    {
        private readonly IDogService _dogService;
        private readonly ILogger _logger;

        public DogController(IDogService dogService, ILogger logger)
        {
            _dogService = dogService;
            _logger = logger;
        }

        [HttpGet("~/dogs")]
        public async Task<IActionResult> Search([FromQuery] DogSearchQuery query)
        {
            _logger.Information("Dog search request: {@DogSearchQuery} on {Created}", query, DateTime.UtcNow);
            var response = await _dogService.SearchDogAsync(query);
            _logger.Information("Dog search response: {@BaseResponse} on {Created}", response, DateTime.UtcNow);
            return Ok(response);
        }

        [HttpPost("~/dogs")]
        public async Task<IActionResult> CreateDog(CreateDog model)
        {
            _logger.Information("Create Dog request: {@CreateDog} on {Created}", model, DateTime.UtcNow);

            var response = await _dogService.AddDogAsync(model);

            if (response.StatusCode == 409)
            {
                _logger.Information("Create Dog Conflict response: {@BaseResponse} on {Created}", response, DateTime.UtcNow);
                return Conflict(response);
            }

            _logger.Information("Create Dog response: {@BaseResponse} on {Created}", response, DateTime.UtcNow);
            return Ok(response);
        }
    }
}
