﻿using CodebridgeTest.Core.Configs;
using CodebridgeTest.Core.Helpers;
using CodebridgeTest.Core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace CodebridgeTest.UIApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PingController : ControllerBase
    {
        private readonly IOptions<AppSettingsConfig> _appSettings;

        public PingController(IOptions<AppSettingsConfig> appSettings)
        {
            _appSettings = appSettings;
        }

        [HttpGet("~/ping")]
        public IActionResult Ping()
        {
            return Ok(new BaseResponse<string> { StatusCode = StatusCodes.Ok, Data = $"Dogs house service. Version {_appSettings.Value.AppVersion}", Message = ResponseUtils.OperationSuccessful });
        }
    }
}
