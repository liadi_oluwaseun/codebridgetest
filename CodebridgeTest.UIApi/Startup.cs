using CodebridgeTest.Core.Configs;
using CodebridgeTest.Core.Helpers;
using CodebridgeTest.Core.Interfaces.Data;
using CodebridgeTest.Core.Interfaces.Service;
using CodebridgeTest.Core.Models;
using CodebridgeTest.Data;
using CodebridgeTest.Data.Repositories;
using CodebridgeTest.Services.Dogs;
using CodebridgeTest.Services.MapProfiles;
using CodebridgeTest.UIApi.Middlewares;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CodebridgeTest.UIApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddDbContext<CodebridgeTestDbContext>((option) => { option.UseSqlite(Configuration.GetConnectionString("DefaultConnection")); });

            services.AddFluentValidation(o =>
            {
                o.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly());
            });

            services.Configure<ApiBehaviorOptions>(options => {
                options.InvalidModelStateResponseFactory = context => {
                    var errors = context.ModelState.Where(e => e.Value.Errors.Count > 0)
                    .SelectMany(x => x.Value.Errors)
                    .Select(x => x.ErrorMessage).ToArray();

                    return new BadRequestObjectResult(new ErrorResponse { StatusCode = StatusCodes.BadRequest, Errors = errors, Message = "" });
                };
            });

            services.Configure<AppSettingsConfig>(Configuration.GetSection("AppSettings"));

            services.AddScoped<IDogService, DogService>();
            services.AddScoped<IDogRepository, DogRepository>();
            services.AddSingleton(Log.Logger);
            services.AddAutoMapper(typeof(CodebridgeTestServiceMapperProfile));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "CodebridgeTest Api", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<ErrorHandlingMiddleware>();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "CodebridgeTest Api V1");
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
