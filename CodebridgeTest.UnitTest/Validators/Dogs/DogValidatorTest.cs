﻿using CodebridgeTest.Core.Entities;
using CodebridgeTest.Core.Models;
using CodebridgeTest.UIApi.Validators;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CodebridgeTest.UnitTest.Validators.Dogs
{
    public class DogValidatorTest
    {
        [Theory]
        [InlineData("red", "jack", 2, 3)]
        [InlineData("yellow", "neo", 4, 10)]
        public async Task CreateDogValidatior_Should_Pass_When_Model_Is_Valid(string color, string name, int tail_length, int weight)
        {
            var createDog = new CreateDog { Color = color, Name = name, TailLength = tail_length, Weight = weight };

            var validator = new CreateDogValidator();

            var result = await validator.ValidateAsync(createDog);

            Assert.True(result.IsValid);
            Assert.Empty(result.Errors);
        }

        [Theory]
        [InlineData("", "jack", 2, 3)]
        [InlineData("yellow", "", 4, 10)]
        [InlineData("", "jack", 0, 3)]
        [InlineData("yellow", "", 4, 0)]
        public async Task CreateDogValidatior_Should_Fail_When_Model_Is_InValid(string color, string name, int tail_length, int weight)
        {
            var createDog = new CreateDog { Color = color, Name = name, TailLength = tail_length, Weight = weight };

            var validator = new CreateDogValidator();

            var result = await validator.ValidateAsync(createDog);

            Assert.False(result.IsValid);
            Assert.NotEmpty(result.Errors);
        }

        [Theory]
        [InlineData("weight", "asc", 1, 100)]
        [InlineData("name", "DESC", 1, 100)]
        [InlineData("tail_length", "ASC", 1, 100)]
        [InlineData("color", "desc", 1, 100)]
        [InlineData("", "", 0, 0)]
        public async Task DogSearchQueryValidator_Should_Pass_When_Model_Is_Valid(string attribute, string order, int pageNum, int pageSize)
        {
            var dogSearchQuery = new DogSearchQuery { Attribute = attribute, Order = order, PageNumber = pageNum, PageSize = pageSize };

            var validator = new DogSearchQueryValidator();

            var result = await validator.ValidateAsync(dogSearchQuery);

            Assert.True(result.IsValid);
            Assert.Empty(result.Errors);
        }

        [Theory]
        [InlineData("weight", "asce", 1, 100)]
        [InlineData("jack", "desc", 1, 100)]
        public async Task DogSearchQueryValidator_Should_Fail_When_Model_Is_InValid(string attribute, string order, int pageNum, int pageSize)
        {
            var dogSearchQuery = new DogSearchQuery { Attribute = attribute, Order = order, PageNumber = pageNum, PageSize = pageSize };

            var validator = new DogSearchQueryValidator();

            var result = await validator.ValidateAsync(dogSearchQuery);

            Assert.False(result.IsValid);
            Assert.NotEmpty(result.Errors);
        }
    }
}
