﻿using AutoMapper;
using CodebridgeTest.Core.Entities;
using CodebridgeTest.Core.Helpers;
using CodebridgeTest.Core.Interfaces.Data;
using CodebridgeTest.Core.Models;
using CodebridgeTest.Services.Dogs;
using Moq;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CodebridgeTest.UnitTest.Services.Dogs
{
    public class DogServiceTest
    {
        [Theory]
        [InlineData("weight", "asc", 1, 100)]
        [InlineData("color", "asc", 1, 100)]
        [InlineData("name", "asc", 1, 100)]
        [InlineData("tail_length", "asc", 1, 100)]
        public async Task SearchDogAsync_Should_Sort_Attribute_In_Asc(string attribute, string order, int pageNum, int pageSize)
        {
            var dogsearchQuery = new DogSearchQuery
            {
                Attribute = attribute,
                Order = order,
                PageNumber = pageNum,
                PageSize = pageSize
            };

            var searchResult = GetAscSearchResult(pageSize, pageNum);

            var dogRepositoryMock = new Mock<IDogRepository>();
            var loggerMock = new Mock<ILogger>();
            var mapperMock = new Mock<IMapper>();

            loggerMock.Setup(l => l.Information(It.IsAny<string>(), It.IsAny<DogSearchDbQuery>(), It.IsAny<DateTime>())).Verifiable();
            loggerMock.Setup(l => l.Information(It.IsAny<string>(), It.IsAny<Paged<Dog>>(), It.IsAny<DateTime>())).Verifiable();

            if (attribute == "weight")
            {
                dogRepositoryMock.Setup(d => d.SearchDogAsync(It.Is<DogSearchDbQuery>(x => x.SortBy == DogSortBy.Weight && x.Order == DogOrder.Asc))).ReturnsAsync(searchResult).Verifiable();
            }
            else if (attribute == "color")
            {
                dogRepositoryMock.Setup(d => d.SearchDogAsync(It.Is<DogSearchDbQuery>(x => x.SortBy == DogSortBy.Color && x.Order == DogOrder.Asc))).ReturnsAsync(searchResult).Verifiable();
            }
            else if (attribute == "name")
            {
                dogRepositoryMock.Setup(d => d.SearchDogAsync(It.Is<DogSearchDbQuery>(x => x.SortBy == DogSortBy.Name && x.Order == DogOrder.Asc))).ReturnsAsync(searchResult).Verifiable();
            }
            else if (attribute == "tail_length")
            {
                dogRepositoryMock.Setup(d => d.SearchDogAsync(It.Is<DogSearchDbQuery>(x => x.SortBy == DogSortBy.TailLenght && x.Order == DogOrder.Asc))).ReturnsAsync(searchResult).Verifiable();
            }

            var service = new DogService(dogRepositoryMock.Object, mapperMock.Object, loggerMock.Object);

            var result = await service.SearchDogAsync(dogsearchQuery);

            dogRepositoryMock.Verify();

            if (attribute == "weight")
            {
                Assert.True(searchResult.Items[0].Weight <= searchResult.Items[1].Weight);
            }
            else if (attribute == "color")
            {
                Assert.True(searchResult.Items[0].Color.CompareTo(searchResult.Items[1].Color) == -1);
            }
            else if (attribute == "name")
            {
                Assert.True(searchResult.Items[0].Name.CompareTo(searchResult.Items[1].Name) == -1);
            }
            else if (attribute == "tail_length")
            {
                Assert.True(searchResult.Items[0].TailLength <= searchResult.Items[1].TailLength);

            }

        }

        [Theory]
        [InlineData("weight", "desc", 1, 100)]
        [InlineData("color", "desc", 1, 100)]
        [InlineData("name", "desc", 1, 100)]
        [InlineData("tail_length", "desc", 1, 100)]
        public async Task SearchDogAsync_Should_Sort_Attribute_In_Desc(string attribute, string order, int pageNum, int pageSize)
        {
            var dogsearchQuery = new DogSearchQuery
            {
                Attribute = attribute,
                Order = order,
                PageNumber = pageNum,
                PageSize = pageSize
            };

            var searchResult = GetDescSearchResult(pageSize, pageNum);

            var dogRepositoryMock = new Mock<IDogRepository>();
            var loggerMock = new Mock<ILogger>();
            var mapperMock = new Mock<IMapper>();

            loggerMock.Setup(l => l.Information(It.IsAny<string>(), It.IsAny<DogSearchDbQuery>(), It.IsAny<DateTime>())).Verifiable();
            loggerMock.Setup(l => l.Information(It.IsAny<string>(), It.IsAny<Paged<Dog>>(), It.IsAny<DateTime>())).Verifiable();

            if (attribute == "weight")
            {
                dogRepositoryMock.Setup(d => d.SearchDogAsync(It.Is<DogSearchDbQuery>(x => x.SortBy == DogSortBy.Weight && x.Order == DogOrder.Desc))).ReturnsAsync(searchResult).Verifiable();
            }
            else if (attribute == "color")
            {
                dogRepositoryMock.Setup(d => d.SearchDogAsync(It.Is<DogSearchDbQuery>(x => x.SortBy == DogSortBy.Color && x.Order == DogOrder.Desc))).ReturnsAsync(searchResult).Verifiable();
            }
            else if (attribute == "name")
            {
                dogRepositoryMock.Setup(d => d.SearchDogAsync(It.Is<DogSearchDbQuery>(x => x.SortBy == DogSortBy.Name && x.Order == DogOrder.Desc))).ReturnsAsync(searchResult).Verifiable();
            }
            else if (attribute == "tail_length")
            {
                dogRepositoryMock.Setup(d => d.SearchDogAsync(It.Is<DogSearchDbQuery>(x => x.SortBy == DogSortBy.TailLenght && x.Order == DogOrder.Desc))).ReturnsAsync(searchResult).Verifiable();
            }

            var service = new DogService(dogRepositoryMock.Object, mapperMock.Object, loggerMock.Object);

            var result = await service.SearchDogAsync(dogsearchQuery);

            dogRepositoryMock.Verify();

            if (attribute == "weight")
            {
                Assert.True(searchResult.Items[0].Weight >= searchResult.Items[1].Weight);
            }
            else if (attribute == "color")
            {
                Assert.True(searchResult.Items[0].Color.CompareTo(searchResult.Items[1].Color) == 1);
            }
            else if (attribute == "name")
            {
                Assert.True(searchResult.Items[0].Name.CompareTo(searchResult.Items[1].Name) == 1);
            }
            else if (attribute == "tail_length")
            {
                Assert.True(searchResult.Items[0].TailLength >= searchResult.Items[1].TailLength);

            }

        }

        [Fact]
        public async Task CreateDogAsync_Return_Success()
        {
            var createDog = GetCreateDogData();
            var createdDogId = Guid.NewGuid();

            var dogRepositoryMock = new Mock<IDogRepository>();
            var loggerMock = new Mock<ILogger>();
            var mapperMock = new Mock<IMapper>();

            dogRepositoryMock.Setup(r => r.GetAsync(It.IsAny<Expression<Func<Dog, bool>>>())).ReturnsAsync(default(Dog));

            dogRepositoryMock.Setup(r => r.AddAsync(It.Is<Dog>(x => x.Name == createDog.Name && x.Color == createDog.Color && x.TailLength == createDog.TailLength && x.Weight == createDog.Weight))).ReturnsAsync(createdDogId).Verifiable();

            loggerMock.Setup(l => l.Information(It.IsAny<string>(), It.Is<Dog>(x => x.Name == createDog.Name && x.Color == createDog.Color && x.TailLength == createDog.TailLength && x.Weight == createDog.Weight), It.IsAny<DateTime>())).Verifiable();

            var service = new DogService(dogRepositoryMock.Object, mapperMock.Object, loggerMock.Object);

            var response = await service.AddDogAsync(createDog);

            dogRepositoryMock.Verify();
            loggerMock.Verify();

            Assert.Equal(200, response.StatusCode);
            Assert.Equal(createdDogId, response.Data);
            Assert.Equal(ResponseUtils.OperationSuccessful, response.Message);
        }

        [Fact]
        public async Task CreateDogAsync_Not_Successful_Returns_409_When_Dog_Name_Already_Exsist()
        {
            var createDog = GetCreateDogData();
            var createdDogId = Guid.NewGuid();

            var dogRepositoryMock = new Mock<IDogRepository>();
            var loggerMock = new Mock<ILogger>();
            var mapperMock = new Mock<IMapper>();

            dogRepositoryMock.Setup(r => r.GetAsync(It.IsAny<Expression<Func<Dog, bool>>>()))
                .ReturnsAsync(new Dog { Id = Guid.NewGuid(), Name = createDog.Name });

            loggerMock.Setup(l => l.Information(It.IsAny<string>(), It.Is<string>(x => x == createDog.Name))).Verifiable();

            var service = new DogService(dogRepositoryMock.Object, mapperMock.Object, loggerMock.Object);

            var response = await service.AddDogAsync(createDog);

            dogRepositoryMock.Verify();
            loggerMock.Verify();

            Assert.Equal(409, response.StatusCode);
            Assert.Equal($"Dog with name: {createDog.Name} already exist", response.Message);
        }

        private CreateDog GetCreateDogData()
        {
            return new CreateDog
            {
                Color = "red",
                Name = "Jack",
                TailLength = 6,
                Weight = 20
            };
        }

        private Paged<Dog> GetAscSearchResult(int pageSize, int pageNum)
        {
            return new Paged<Dog>
            {
                PageNumber = pageNum,
                PageSize = pageSize,
                TotalCount = 2,
                Items = new List<Dog>
                {
                    new Dog { Id = Guid.NewGuid(), Name = "act", Color = "blue", TailLength = 6, Weight = 20},
                    new Dog { Id = Guid.NewGuid(), Name = "bat", Color = "yellow", TailLength = 8, Weight = 30}
                }
            };
        }

        private Paged<Dog> GetDescSearchResult(int pageSize, int pageNum)
        {
            return new Paged<Dog>
            {
                PageNumber = pageNum,
                PageSize = pageSize,
                TotalCount = 2,
                Items = new List<Dog>
                {
                    new Dog { Id = Guid.NewGuid(), Name = "bat", Color = "yellow", TailLength = 8, Weight = 30},
                    new Dog { Id = Guid.NewGuid(), Name = "act", Color = "blue", TailLength = 6, Weight = 20}
                }
            };
        }
    }
}
