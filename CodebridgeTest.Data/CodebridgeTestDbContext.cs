﻿using CodebridgeTest.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace CodebridgeTest.Data
{
    public class CodebridgeTestDbContext : DbContext
    {
        public CodebridgeTestDbContext(DbContextOptions<CodebridgeTestDbContext> options)
            :base(options)
        {

        }

        public DbSet<Dog> Dogs { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            //modelBuilder.Entity<Dog>(d => {
            //    d.HasKey(x => x.Id);

            //    d.Property(x => x.Name).IsRequired().HasMaxLength(100);

            //    d.Property(x => x.Color).IsRequired().HasMaxLength(100);

            //    d.HasData(new List<Dog> {
            //        new Dog { Id = Guid.NewGuid(), Name = "Neo", Color = "red & amber", TailLength = 22, Weight = 33},
            //        new Dog {Id = Guid.NewGuid(), Name = "Jessy", Color = "black & white", TailLength = 7, Weight = 14 }
            //    });
            //});

            // base.OnModelCreating(modelBuilder);
        }

    }
}
