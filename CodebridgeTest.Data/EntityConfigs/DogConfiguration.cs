﻿using CodebridgeTest.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodebridgeTest.Data.EntityConfigs
{
    public class DogConfiguration : IEntityTypeConfiguration<Dog>
    {
        public void Configure(EntityTypeBuilder<Dog> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Name).IsRequired().HasMaxLength(100);

            builder.Property(x => x.Color).IsRequired().HasMaxLength(100);

            builder.HasData(new List<Dog> {
                    new Dog { Id = Guid.NewGuid(), CreatedDateUtc = DateTime.UtcNow, LastModifiedDateUtc = DateTime.UtcNow, Name = "Neo", Color = "red & amber", TailLength = 22, Weight = 33},
                    new Dog {Id = Guid.NewGuid(),  CreatedDateUtc = DateTime.UtcNow, LastModifiedDateUtc = DateTime.UtcNow, Name = "Jessy", Color = "black & white", TailLength = 7, Weight = 14 }
                });
        }
    }
}
