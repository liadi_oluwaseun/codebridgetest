﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CodebridgeTest.Data.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Dogs",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDateUtc = table.Column<DateTime>(nullable: false),
                    LastModifiedDateUtc = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Color = table.Column<string>(maxLength: 100, nullable: false),
                    TailLength = table.Column<int>(nullable: false),
                    Weight = table.Column<int>(nullable: false),
                    DeletedDateUtc = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dogs", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Dogs",
                columns: new[] { "Id", "Color", "CreatedDateUtc", "DeletedDateUtc", "LastModifiedDateUtc", "Name", "TailLength", "Weight" },
                values: new object[] { new Guid("a49366b2-b72c-4a50-b412-1c54bc06b99a"), "red & amber", new DateTime(2022, 1, 11, 8, 36, 56, 936, DateTimeKind.Utc).AddTicks(1628), null, new DateTime(2022, 1, 11, 8, 36, 56, 936, DateTimeKind.Utc).AddTicks(2191), "Neo", 22, 33 });

            migrationBuilder.InsertData(
                table: "Dogs",
                columns: new[] { "Id", "Color", "CreatedDateUtc", "DeletedDateUtc", "LastModifiedDateUtc", "Name", "TailLength", "Weight" },
                values: new object[] { new Guid("d216f720-e90e-4e59-af8d-328d95b44430"), "black & white", new DateTime(2022, 1, 11, 8, 36, 56, 936, DateTimeKind.Utc).AddTicks(4926), null, new DateTime(2022, 1, 11, 8, 36, 56, 936, DateTimeKind.Utc).AddTicks(4955), "Jessy", 7, 14 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Dogs");
        }
    }
}
