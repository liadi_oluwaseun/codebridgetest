﻿using CodebridgeTest.Core.Entities;
using CodebridgeTest.Core.Interfaces.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CodebridgeTest.Data.Repositories
{
    public abstract class DataRepository<T> : IDataRepository<T> where T: BaseEntity
    {
        private readonly CodebridgeTestDbContext _dbContext;

        protected DataRepository(CodebridgeTestDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IList<T>> FindAllAsync()
        {
           return await  _dbContext.Set<T>().ToListAsync();
        }

        public async Task<T> GetAsync(Guid id)
        {
            return await _dbContext.Set<T>().FindAsync(id);
        }

        public async Task<T> GetAsync(Expression<Func<T, bool>> expression)
        {
            var entity = await _dbContext.Set<T>().Where(expression).SingleOrDefaultAsync();

            return entity;
        }

        public async Task<Guid> AddAsync(T entity)
        {
            await _dbContext.Set<T>().AddAsync(entity);
            await _dbContext.SaveChangesAsync();

            return entity.Id;
        }
    }
}
