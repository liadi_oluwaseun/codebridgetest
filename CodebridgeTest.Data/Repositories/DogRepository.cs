﻿using CodebridgeTest.Core.Entities;
using CodebridgeTest.Core.Exceptions;
using CodebridgeTest.Core.Interfaces.Data;
using CodebridgeTest.Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CodebridgeTest.Data.Repositories
{
    public class DogRepository: DataRepository<Dog>, IDogRepository
    {
        private readonly CodebridgeTestDbContext _dbContext;

        public DogRepository(CodebridgeTestDbContext dbContext)
            : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Paged<Dog>> SearchDogAsync(DogSearchDbQuery dogSearchDbQuery)
        {
            var dogsAsQueryable =  _dbContext.Dogs.AsQueryable(); 

            Expression<Func<Dog, Object>> filter = default;

            if (dogSearchDbQuery.SortBy.HasValue)
            {
                switch (dogSearchDbQuery.SortBy.Value)
                {
                    case DogSortBy.Name:
                        filter = (d) => d.Name;
                        break;
                    case DogSortBy.Color:
                        filter = (d) => d.Color;
                        break;
                    case DogSortBy.TailLenght:
                        filter = (d) => (uint)d.TailLength;
                        break;
                    case DogSortBy.Weight:
                        filter = (d) => d.Weight;
                        break;
                }
            }
            else
            {
                filter = (d) => d.CreatedDateUtc;
            }


            if (dogSearchDbQuery.Order.HasValue)
            {
                switch (dogSearchDbQuery.Order.Value)
                {
                    case DogOrder.Desc:
                        dogsAsQueryable = dogsAsQueryable.OrderByDescending(filter);
                        break;
                    case DogOrder.Asc:
                        dogsAsQueryable = dogsAsQueryable.OrderBy(filter);
                        break;
                    default:
                        break;
                }
            }
            else
            {
                dogsAsQueryable = dogsAsQueryable.OrderBy(filter);
            }

            var totalCount = dogsAsQueryable?.Count() ?? default(long);
            var results = await dogsAsQueryable.Skip(dogSearchDbQuery.PageSize * (dogSearchDbQuery.PageNumber - 1)).Take(dogSearchDbQuery.PageSize).ToListAsync();

            return new Paged<Dog> { PageSize = dogSearchDbQuery.PageSize, PageNumber = dogSearchDbQuery.PageNumber, TotalCount = totalCount, Items = results };

        }
    }
}
