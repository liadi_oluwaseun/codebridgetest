﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodebridgeTest.Core.Configs
{
    public class AppSettingsConfig
    {
        public string AppVersion { get; set; }
    }
}
