﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodebridgeTest.Core.Exceptions
{
    public class AppException : ApplicationException
    {
        public AppException(int statusCode, string friendlyMessage)
        {
            StatusCode = statusCode;
            FriendlyMessage = friendlyMessage;
        }

        public int StatusCode { get; set; }

        public string FriendlyMessage { get; set; }
    }
}
