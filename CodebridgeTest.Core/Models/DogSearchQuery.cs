﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodebridgeTest.Core.Models
{
    public class DogSearchQuery :PagedQuery
    {
        public string Attribute { get; set; }

        public string Order { get; set; }
    }
}
