﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodebridgeTest.Core.Models
{
    public enum DogSortBy
    {
        Name,
        Color,
        TailLenght,
        Weight
    }
}
