﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodebridgeTest.Core.Models
{
    public class DogSearchDbQuery
    {
        public int PageSize { get; set; }

        public int PageNumber { get; set; }

        public DogSortBy? SortBy { get; set; }

        public DogOrder? Order { get; set; }
    }
}
