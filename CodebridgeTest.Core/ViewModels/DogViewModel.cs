﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace CodebridgeTest.Core.ViewModels
{
    public class DogViewModel
    {
        public string Name { get; set; }

        public string Color { get; set; }

        [JsonPropertyName("tail_length")]
        public int TailLength { get; set; }

        public int Weight { get; set; }
    }
}
