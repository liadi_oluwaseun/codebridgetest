﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodebridgeTest.Core.Helpers
{
    public static class ResponseUtils
    {
        public static string OperationSuccessful = "Operation successful";
    }

    public static class StatusCodes
    {
        public static int Ok = 200;
        public static int internalServerError = 500;
        public static int BadRequest = 400;
        public static int Conflict = 409;
    }
}
