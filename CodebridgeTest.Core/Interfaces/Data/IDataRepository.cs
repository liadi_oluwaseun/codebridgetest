﻿using CodebridgeTest.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CodebridgeTest.Core.Interfaces.Data
{
    public interface IDataRepository<T> where T: BaseEntity
    {
        Task<Guid> AddAsync(T entity);

        Task<T> GetAsync(Guid id);

        Task<T> GetAsync(Expression<Func<T, bool>> expression);

        Task<IList<T>> FindAllAsync();
    }
}
