﻿using CodebridgeTest.Core.Entities;
using CodebridgeTest.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CodebridgeTest.Core.Interfaces.Data
{
    public interface IDogRepository: IDataRepository<Dog>
    {
        Task<Paged<Dog>> SearchDogAsync(DogSearchDbQuery dogSearchDbQuery);
    }
}
