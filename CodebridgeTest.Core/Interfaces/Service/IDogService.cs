﻿using CodebridgeTest.Core.Entities;
using CodebridgeTest.Core.Models;
using CodebridgeTest.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CodebridgeTest.Core.Interfaces.Service
{
    public interface IDogService
    {
        Task<BaseResponse<Guid>> AddDogAsync(CreateDog createDog);

        Task<BaseResponse<Paged<DogViewModel>>> SearchDogAsync(DogSearchQuery dogSearchQuery);
    }
}
