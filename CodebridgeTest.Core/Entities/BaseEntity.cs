﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodebridgeTest.Core.Entities
{
    public class BaseEntity
    {
        public Guid Id { get; set; }

        public DateTime CreatedDateUtc { get; set; }

        public DateTime? LastModifiedDateUtc { get; set; }
    }
}
