﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace CodebridgeTest.Core.Entities
{
    public class Dog: BaseEntity
    {
        public string Name { get; set; }

        public string Color { get; set; }

        public int TailLength { get; set; }

        public int Weight { get; set; }

        public DateTime? DeletedDateUtc { get; set; }
    }
}
