# README #

# Instructions to build and run this project

1. Install visual studio.

2. Open CodebridgeTest.sln in visual studio.

3. Open solution explorer, right click on solution and click restore nuget packages.

4. Set Startup project to CodebridgeTest.UIApi

5. From Package Manager Console

        * Set default project to CodebridgeTest.Data 
        * Run update-database -migration InitialMigration, to create 
          database and table.

6. Run the CodebridgeTest.UIApi project

###### Pls contact if you face any issue while setting up my solution
