﻿using AutoMapper;
using CodebridgeTest.Core.Entities;
using CodebridgeTest.Core.Models;
using CodebridgeTest.Core.ViewModels;

namespace CodebridgeTest.Services.MapProfiles
{
    public class CodebridgeTestServiceMapperProfile: Profile
    {
        public CodebridgeTestServiceMapperProfile()
        {
            CreateMap<Dog, DogViewModel>();
            CreateMap<Paged<Dog>, Paged<DogViewModel>>();
        }
    }
}
