﻿using AutoMapper;
using CodebridgeTest.Core.Entities;
using CodebridgeTest.Core.Helpers;
using CodebridgeTest.Core.Interfaces.Data;
using CodebridgeTest.Core.Interfaces.Service;
using CodebridgeTest.Core.Models;
using CodebridgeTest.Core.ViewModels;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CodebridgeTest.Services.Dogs
{
    public class DogService: IDogService
    {
        private readonly IDogRepository _dogRepository;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public DogService(IDogRepository dogRepository, IMapper mapper, ILogger logger)
        {
            _dogRepository = dogRepository;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<BaseResponse<Guid>> AddDogAsync(CreateDog createDog)
        {
            var existingDog = await _dogRepository.GetAsync(d => d.Name.ToLower() == createDog.Name.Trim().ToLower());

            if (existingDog != null)
            {
                _logger.Information("Dog with name: {@string} already exist", createDog.Name);

                return new BaseResponse<Guid>
                {
                    StatusCode = StatusCodes.Conflict,
                    Message = $"Dog with name: {createDog.Name} already exist"
                };
            }

            var dog = new Dog
            {
                Id = Guid.NewGuid(),
                Color = createDog.Color,
                CreatedDateUtc = DateTime.UtcNow,
                LastModifiedDateUtc = DateTime.UtcNow,
                Name = createDog.Name,
                TailLength = createDog.TailLength,
                Weight = createDog.Weight
            };

            var id = await _dogRepository.AddAsync(dog);

            _logger.Information("Dog created dog: {@Dog} on {@Created}", dog, DateTime.UtcNow);

            return new BaseResponse<Guid>
            {
                StatusCode = StatusCodes.Ok,
                Data = id,
                Message = ResponseUtils.OperationSuccessful
            };
        }

        public async Task<BaseResponse<Paged<DogViewModel>>> SearchDogAsync(DogSearchQuery dogSearchQuery)
        {
            var dbQuery = ToDogSearchDbQuery(dogSearchQuery);
            _logger.Information("Convert to DogSearchDbQuery result: {@DogSearchDbQuery} on {Created}", dbQuery, DateTime.UtcNow);

            var result = await _dogRepository.SearchDogAsync(dbQuery);

            _logger.Information("Dog search db search result: {@Paged} on {Created}", result, DateTime.UtcNow);

            var viewModel = _mapper.Map<Paged<DogViewModel>>(result);

            return new BaseResponse<Paged<DogViewModel>>
            {
                StatusCode = StatusCodes.Ok,
                Data = viewModel,
                Message = ResponseUtils.OperationSuccessful
            };
        }

        private DogSearchDbQuery ToDogSearchDbQuery(DogSearchQuery dogSearchQuery)
        {
            var dbQuery = new DogSearchDbQuery();
            dbQuery.PageNumber = dogSearchQuery.PageNumber;
            dbQuery.PageSize = dogSearchQuery.PageSize;

            if (!string.IsNullOrEmpty(dogSearchQuery.Order))
            {
                switch (dogSearchQuery.Order.ToLower())
                {
                    case "asc":
                        dbQuery.Order = DogOrder.Asc;
                        break;
                    case "desc":
                        dbQuery.Order = DogOrder.Desc;
                        break;
                    default:
                        break;
                }
            }

            if (!string.IsNullOrEmpty(dogSearchQuery.Attribute))
            {
                switch (dogSearchQuery.Attribute.ToLower())
                {
                    case "name":
                        dbQuery.SortBy = DogSortBy.Name;
                        break;
                    case "color":
                        dbQuery.SortBy = DogSortBy.Color;
                        break;
                    case "tail_length":
                        dbQuery.SortBy = DogSortBy.TailLenght;
                        break;
                    case "weight":
                        dbQuery.SortBy = DogSortBy.Weight;
                        break;
                    default:
                        break;
                }
            }

            return dbQuery;
        }
    }
}
